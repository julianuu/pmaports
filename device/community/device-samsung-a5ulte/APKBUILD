# Reference: <https://postmarketos.org/devicepkg>
# Maintainer: Minecrell <minecrell@minecrell.net>
pkgname=device-samsung-a5ulte
pkgdesc="Samsung Galaxy A5 (SM-A500FU)"
pkgver=4
pkgrel=0
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
depends="postmarketos-base mkbootimg soc-qcom-msm8916"
makedepends="devicepkg-dev"
source="deviceinfo"
subpackages="
	$pkgname-kernel-mainline:kernel_mainline
	$pkgname-kernel-mainline-modem:kernel_mainline_modem
	$pkgname-nonfree-firmware:nonfree_firmware
	$pkgname-nonfree-firmware-modem:nonfree_firmware_modem
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
}

kernel_mainline() {
	pkgdesc="Close to mainline kernel (no modem, audio routed directly)"
	depends="linux-postmarketos-qcom-msm8916"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

kernel_mainline_modem() {
	pkgdesc="Close to mainline kernel (non-free modem, audio routed through ADSP)"
	depends="linux-postmarketos-qcom-msm8916 soc-qcom-msm8916-modem"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

nonfree_firmware() {
	pkgdesc="GPU/WiFi/BT/Video(/Modem) firmware"
	depends="linux-firmware-qcom firmware-samsung-a5ulte-venus firmware-samsung-a5ulte-wcnss"
	mkdir "$subpkgdir"
}

nonfree_firmware_modem() {
	pkgdesc="Modem firmware"
	depends="firmware-samsung-a5ulte-modem"
	install_if="$pkgname-nonfree-firmware $pkgname-kernel-mainline-modem"
	mkdir "$subpkgdir"
}

sha512sums="e229166e699a5ca68fd5c762b70a25ff8306986d63190a5d9dba7bc296dec5d597d660a37b25a0f39b4722a595678aa0ac35695e50a18a757436e9ad6a2c55e9  deviceinfo"
