# Reference: <https://postmarketos.org/vendorkernel>
# Maintainer:  Clayton Craft <clayton@craftyguy.net>
# Co-Maintainer: Bhushan Shah <bshah@kde.org>
pkgname=linux-purism-librem5
pkgver=5.12.2
pkgrel=0
_purismrel=1
# <kernel ver>.<purism kernel release>
_purismver=${pkgver}pureos$_purismrel
pkgdesc="Purism Librem 5 phone kernel fork"
arch="aarch64"
_carch="arm64"
_flavor="purism-librem5"
url="https://source.puri.sm/Librem5/linux-next"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="
	bison
	devicepkg-dev
	findutils
	flex
	installkernel
	openssl-dev
	perl
	rsync
	xz
	"
subpackages="$pkgname-dev"
install="$pkgname.post-upgrade"

# Source
_repository="linux-next"
# kconfig generated with: ARCH=arm64 make defconfig KBUILD_DEFCONFIG=librem5_defconfig
_config="config-$_flavor.$arch"
source="
	$pkgname-$_purismver.tar.gz::https://source.puri.sm/Librem5/$_repository/-/archive/pureos/$_purismver/$_repository-pureos-$_purismver.tar.gz
	0001-bq25890_charger-enter-ship-mode-on-power-off.patch
	0002-arm64-dts-imx8mq-librem5.dtsi-adjust-the-usdhc-bus-s.patch
	0003-imx8mq-librem5-r3-Set-the-CPU-voltage-to-1.0V-when-r.patch
	0004-arm64-dts-imx8mq-disable-SuperSpeed-instances-in-par.patch
	$_config
"
builddir="$srcdir/$_repository-pureos-$_purismver"

prepare() {
	default_prepare
	REPLACE_GCCH=0 \
		. downstreamkernel_prepare
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS" \
		LOCALVERSION=".$_purismrel"
}

package() {
	downstreamkernel_package "$builddir" "$pkgdir" "$_carch" "$_flavor"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"

}

dev() {
	provides="linux-headers"
	replaces="linux-headers"

	cd $builddir

	# https://github.com/torvalds/linux/blob/master/Documentation/kbuild/headers_install.rst
	make -j1 headers_install \
		ARCH="$_carch" \
		INSTALL_HDR_PATH="$subpkgdir"/usr
}
sha512sums="
b15616545a479c574d3f214cd3c1571998fe4663d4f4f03c2d88d701d5486ee97e4e742597e9d05a136d14141c4809c76cd28e436c2532179759818a6494e8e0  linux-purism-librem5-5.12.2pureos1.tar.gz
c96ce17c2c90ba89e44aeca392d9df42d8d5f198bc86ec70ce7d112e3f01b6e74ce5805711b5006f48740c789a5368354c2c7fb484a6f5bbe6bc98d0e72758da  0001-bq25890_charger-enter-ship-mode-on-power-off.patch
63ffa8e40990204f944ab67489fc57bc35e9f21f3c0c3a8d2f5eebba31eb1e4ed31d5973e21d83081f32289ad234e3a5d01c25ebdf5ade6d20595cba4106bf86  0002-arm64-dts-imx8mq-librem5.dtsi-adjust-the-usdhc-bus-s.patch
333a2370d9663a162501338fca0fd7939d7b132ecb738d716047bb8cb0635f458d80ce0d758170211b2233361d0fb3c7266a51e68e7c2307eaba3e52f1084bfe  0003-imx8mq-librem5-r3-Set-the-CPU-voltage-to-1.0V-when-r.patch
35208c873b0dc3084a9b336ba4ad61afb20d63a7fdffe53be84e9c4da10734ba015b564d7d238df82919b30f52aac266cb3e78797a8304af61ddcd943a729e3c  0004-arm64-dts-imx8mq-disable-SuperSpeed-instances-in-par.patch
114e005319e4596328c274ab2cf669082d772fc352d3cbfb17a736fe1799c70c607f6620379bdeff96c164a69ef3b0632f781aa33589cddc84118e03a07cff0e  config-purism-librem5.aarch64
"
